function clicked() {
    let f1 = document.getElementsByName("field1");
    let f2 = document.getElementsByName("field2");
    let r = document.getElementById("result");
    if (!(/^[0-9]+$/).test(f1[0].value) || !(/^[0-9]+$/).test(f2[0].value)) {
        window.alert("Пожалуйста, вводите только числа!");
        return false;
    }
    r.innerHTML = "Сумма " + (f1[0].value * f2[0].value);
    return false;

}
/*function clicked() {
    let f1 = document.getElementsByName("field1");
    let f2 = document.getElementsByName("field2");
    let r = document.getElementById("result");
    if (!(/^[0-9]+$/).test(f1[0].value) || !(/^[0-9]+$/).test(f2[0].value)) {
        window.alert("Пожалуйста, вводите только числа!");
        return false;
    }

    return false;
} */
window.addEventListener("DOMContentLoaded", function () {
    //console.log("DOM fully loaded and parsed");
    let b = document.getElementById("button1");
    b.addEventListener("click", clicked);
    let r = document.getElementById("result");
    r.innerHTML = "Результат будет тут";
});
